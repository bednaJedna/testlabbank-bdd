'''
Collection of common utility funcs.
'''

import json


def load_file(path_with_filename: str, type_=None):
    '''
    Loads file of specified type (e.g. .csv, .json,...)
    :param path_with_filename:
    :param type_:
    :return:
    '''
    with open(
        ''.join(
            [path_with_filename, '.', type_]
        ), mode='r'
    ) as file:
        output = json.load(file, encoding='utf-8')

    return output
