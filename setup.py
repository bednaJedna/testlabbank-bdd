from setuptools import setup, find_packages

setup(
    name='testlabbank-bdd',
    version='0.0.1',
    author='bednaJedna',
    author_email='radek.bednarik@tesena.com',
    packages=find_packages(),
    install_requires=[
        'selenium',
        'behave'
    ]
)
