from sys import platform
from behave import fixture, use_fixture
from selenium.webdriver import Chrome
from selenium.webdriver.support.wait import WebDriverWait

from resources.helpers.utils import load_file

CONFIGURATION = load_file('../resources/variables/config', type_='json')


@fixture
def driver_chrome(context):
    '''
    SetsUp and TearsDown environment for all Scenarios
    :param context:
    :return:
    '''
    # Set Up
    context.confs = CONFIGURATION
    if platform.startswith('win32'):
        context.driver = Chrome(
            executable_path=context.confs['selenium']['chromedriver_win'])
    elif platform.startswith('linux'):
        context.driver = Chrome(
            executable_path=context.confs['selenium']['chromedriver_lin'])
    context.wait = WebDriverWait(context.driver, 15)
    yield context.driver, context.wait, context.confs
    # Tear Down
    context.driver.quit()


def before_scenario(context, scenario):
    '''
    Loads before each scenario.
    :param context, scenario:
    :return:
    '''
    use_fixture(driver_chrome, context)






