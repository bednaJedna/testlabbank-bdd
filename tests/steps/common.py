from time import sleep

from behave import Given, When, Then
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec


@Given('Page "{page}" is Opened')
def step_impl(context, page):
    '''
    Opens the web on given url
    :param context:
    :param page:
    :return:
    '''
    if page == 'testapp':

        url = context.confs['homepage']['homepage_login_url']
        username = context.confs['homepage']['homepage_login_username']
        password = context.confs['homepage']['homepage_login_password']
        context.driver.get(url.format(username, password))


@Given('Element "{element}" is Visible')
def step_impl(context, element):
    '''
    Checks visibility of the href link element
    :param context:
    :param element:
    :return:
    '''
    if element == 'tesena, s.r.o':
        wait = context.wait
        href_ = context.confs['homepage']['homepage_xpath_href_to_tesena']

        context.tesena_url = wait.until(ec.visibility_of_element_located(
            (By.XPATH, href_)
        ))

    elif element == 'address':
        wait = context.wait
        loc_displayed_address = context.confs['homepage']['homepage_xpath_href_tesena_address']

        context.displayed_address_text = wait.until(ec.visibility_of_element_located(
            (By.XPATH, loc_displayed_address)
            )
        ).text

    elif element == 'mailto':
        wait = context.wait
        loc_mailto = context.confs['homepage']['homepage_xpath_to_link_mailto']

        context.mailto_attr = wait.until(ec.visibility_of_element_located(
            (By.XPATH, loc_mailto)
            )
        ).get_attribute('href')


@When('Element "{element}" is Visible')
def step_impl(context, element):
    if element == 'Sign In':
        wait = context.wait
        loc_login_bttn = context.confs['homepage']['homepage_xpath_to_payments_bttn_sign_in']

        context.bttn_sign_in_to_payments = wait.until(ec.visibility_of_element_located(
            (By.XPATH, loc_login_bttn)
        ))


@When('I Click on the Element "{clickable}"')
def step_impl(context, clickable):
    '''
    Clicks on given element
    :param context:
    :param clickable:
    :return:
    '''
    if clickable == 'tesena, s.r.o':
        context.tesena_url.click()

    elif clickable == 'Sign In':
        context.bttn_sign_in_to_payments.click()


@Then('Target Page "{target_page}" is Opened')
def step_impl(context, target_page):
    '''
    Checks, that intended page was opened
    :param context:
    :param page:
    :return:
    '''
    wait = context.wait

    if target_page == 'Tesena Homepage':
        driver = context.driver
        heading = context.confs['homepage']['homepage_xpath_tesena_home_heading']

        sleep(3)
        driver.switch_to.window(driver.window_handles[-1])
        element = wait.until(ec.presence_of_element_located(
            (By.XPATH, heading))
        )

        assert element is not False

    elif target_page == 'Payments Order':
        form_title_correct = context.confs['payments']['payments_form_title_text']
        loc_form_title = context.confs['payments']['payments_xpath_form_title']

        form_title_text = wait.until(ec.visibility_of_element_located(
            (By.XPATH, loc_form_title)
            )
        ).text

        assert form_title_correct.lower().strip() == form_title_text.lower().strip()
