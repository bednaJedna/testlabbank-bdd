from behave import Then

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec

@When('Login "{field}" Field is Filled')
def step_impl(context, field):
    wait = context.wait

    if field == 'email':
        login_email = context.confs['payments']['payments_login_email']
        loc_login_email_field = context.confs['homepage']['homepage_xpath_to_payments_username']

        wait.until(ec.visibility_of_element_located(
            (By.XPATH, loc_login_email_field)
            )
        ).send_keys(login_email)

    elif field == 'password':
        loc_login_pass_field = context.confs['homepage']['homepage_xpath_to_payments_password']
        login_passw = context.confs['payments']['payments_login_password']

        wait.until(ec.visibility_of_element_located(
            (By.XPATH, loc_login_pass_field)
            )
        ).send_keys(login_passw)


@Then('"{title}" is Displayed')
def step_impl(context, title):
    wait = context.wait

    app_title = wait.until(ec.visibility_of_element_located(
        (By.XPATH, context.confs['homepage']['homepage_xpath_title'])
    )).text

    assert title.lower().strip() == app_title.lower().strip()


@Then('Text "{text}" of the Element is Correct')
def step_impl(context, text):
    if text == 'address':
        correct_address = context.confs['homepage']['homepage_tesena_correct_address']

        assert correct_address.lower().strip() == context.displayed_address_text.lower().strip()


@Then('"{attribute}" Attribute is Valid')
def step_impl(context, attribute):
    if attribute == 'mailto':
        mailto_correct_attr = context.confs['homepage']['homepage_link_mailto_correct']
        mailto_real_attr = context.mailto_attr

        assert mailto_correct_attr.lower().strip() == mailto_real_attr.lower().strip()
