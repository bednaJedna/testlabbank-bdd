Feature: Bank App Homepage Tests
  Test Suite containing all tests within the App Homepage.

  Scenario: URL Leads To App Homepage
    Given Page "testapp" is Opened
    Then "VERY SMART BANKING Inc." is Displayed

  Scenario: Link tesena,s.r.o Leads to Tesena Homepage
    Given Page "testapp" is Opened
    And Element "tesena, s.r.o" is Visible
    When I Click on the Element "tesena, s.r.o"
    Then Target Page "Tesena Homepage" is Opened

  Scenario: Company Address Text is Correct
    Given Page "testapp" is Opened
    And Element "address" is Visible
    Then Text "address" of the Element is Correct

  Scenario: Login to Payments is Working
    Given Page "testapp" is Opened
    When Login "email" Field is Filled
    And Login "password" Field is Filled
    And Element "Sign In" is Visible
    And I Click on the Element "Sign In"
    Then Target Page "Payments Order" is Opened

  Scenario: Link "info@tesena.com" is Valid
    Given Page "testapp" is Opened
    And Element "mailto" is Visible
    Then "mailto" Attribute is Valid
